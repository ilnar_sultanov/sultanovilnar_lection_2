package com.transfers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.users.Reciever;
import com.users.Sender;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

@JsonAutoDetect
@JsonPropertyOrder({ "transferNumber", "getDateInSimpleFormat()", "getCurrencyOnRussian()",
        "costOfOperation", "reciever", "sender"})
public abstract class Transfer {

    @JsonProperty("Номер перевода")
    private int transferNumber;

    static int transferCounter = 1;

    @JsonProperty("Сумма")
    private double costOfOperation;

    @JsonProperty("Валюта")
    private Currency currency;

    @JsonProperty("Дата")
    private Date dateOfTransfer;

    @JsonProperty("Отправитель")
    private Sender sender;

    @JsonProperty("Получатель")
    private Reciever reciever;

    @JsonIgnore
    public Transfer(double costOfOperation, Currency currencyOfMoney, Date dateOfTransfer, Sender sender, Reciever reciever) {
        this.transferNumber = transferCounter++;
        this.costOfOperation = costOfOperation;
        this.currency = currencyOfMoney;
        this.dateOfTransfer = dateOfTransfer;
        this.sender = sender;
        this.reciever = reciever;
    }

    @JsonIgnore
    public void doTransfer(){
        if (sender.isMoneyMoreThan(getCostOfOperation())) {
            sender.setMoney(sender.getMoney() - getCostOfOperation());
            reciever.setMoney(reciever.getMoney() + getCostOfOperation());
            System.out.printf(getTransferInfo());
        } else System.out.println("Перевод совершить нельзя, недостаточно средств");
    }

    @JsonIgnore
    public String getJsonString(){
        if (getSender().isMoneyMoreThan(getCostOfOperation())) {
            ObjectMapper mapper = new ObjectMapper();

            try {
                return mapper.writeValueAsString(this);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return "Перевод совершить нельзя, недостаточно средств";
    }

    @JsonIgnore
    public abstract String getTransferInfo();

    @JsonIgnore
    public int getTransferNumber() {
        return transferNumber;
    }

    @JsonIgnore
    public double getCostOfOperation() {
        return costOfOperation;
    }

    @JsonIgnore
    public void setCostOfOperation(long costOfOperation) {
        this.costOfOperation = costOfOperation;
    }

    @JsonIgnore
    public Currency getCurrency() {
        return currency;
    }

    @JsonProperty("Валюта")
    public String getCurrencyOnRussian() {
        return currency.getRussian();
    }

    @JsonIgnore
    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    @JsonIgnore
    public Date getDateOfTransfer() {
        return dateOfTransfer;
    }

    @JsonProperty("Дата")
    public String getDateInSimpleFormat() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        return formatter.format(dateOfTransfer);
    }

    @JsonIgnore
    public void setDateOfTransfer(Date dateOfTransfer) {
        this.dateOfTransfer = dateOfTransfer;
    }

    @JsonIgnore
    public Sender getSender() {
        return sender;
    }

    @JsonIgnore
    public void setSender(Sender sender) {
        this.sender = sender;
    }

    @JsonIgnore
    public Reciever getReciever() {
        return reciever;
    }

    @JsonIgnore
    public void setReciever(Reciever reciever) {
        this.reciever = reciever;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transfer transfer = (Transfer) o;
        return transferNumber == transfer.transferNumber &&
                costOfOperation == transfer.costOfOperation &&
                Objects.equals(currency, transfer.currency) &&
                Objects.equals(dateOfTransfer, transfer.dateOfTransfer) &&
                Objects.equals(sender, transfer.sender) &&
                Objects.equals(reciever, transfer.reciever);
    }

    @Override
    public int hashCode() {
        return Objects.hash(transferNumber, costOfOperation, currency, dateOfTransfer, sender, reciever);
    }
}
