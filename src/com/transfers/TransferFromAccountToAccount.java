package com.transfers;

import com.users.Reciever;
import com.users.Sender;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Date;

public class TransferFromAccountToAccount extends Transfer {

    public TransferFromAccountToAccount(long costOfOperation, Currency currencyOfMoney, Date dateOfTransfer, Sender sender, Reciever reciever) {
        super(costOfOperation, currencyOfMoney, dateOfTransfer, sender, reciever);
    }

    @Override
    public String getTransferInfo() {
        return String.format("ПЕРЕВОД СО СЧЕТА НА СЧЕТ: " +
                        "Номер перевода: %d, " +
                        "Дата: %s, " +
                        "Номер счета получателя: %s, " +
                        "Номер счета отправителя: %s, " +
                        "Валюта: %s, " +
                        "Сумма: %s, " +
                        "Получатель: %s, " +
                        "Отправитель: %s.\n",
                getTransferNumber(), getDateInSimpleFormat(), getReciever().getNumberOfAccount(),
                getSender().getNumberOfAccount(), getCurrency().getRussian(),
                getCostOfOperation(), getReciever().getFullName(), getSender().getFullName());
    }
}
