package com.transfers;

import com.users.Reciever;
import com.users.Sender;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Date;

public class TransferFromCardToCard extends Transfer {

    public TransferFromCardToCard(long costOfOperation, Currency currencyOfMoney, Date dateOfTransfer, Sender sender, Reciever recipient) {
        super(costOfOperation, currencyOfMoney, dateOfTransfer, sender, recipient);
    }

    @Override
    public String getTransferInfo() {
        return String.format("ПЕРЕВОД С КАРТЫ НА КАРТУ: " +
                        "Номер перевода: %d, " +
                        "Дата: %s, " +
                        "Номер карты получателя: %s, " +
                        "Номер карты отправителя: %s, " +
                        "Валюта: %S, " +
                        "Сумма: %s, " +
                        "Получатель: %s, " +
                        "Отправитель: %s.\n",
                getTransferNumber(), getDateInSimpleFormat(), getReciever().getNumberOfCard(),
                getSender().getNumberOfCard(), getCurrency().getRussian(), getCostOfOperation(),
                getReciever().getFullName(), getSender().getFullName());
    }
}
