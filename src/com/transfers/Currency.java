package com.transfers;

public enum Currency {
    DOLLAR("ДОЛЛАР"),
    EURO("ЕВРО"),
    RUBLE("РУБЛЬ");

    private final String translation;

    Currency(String translation) {
        this.translation = translation;
    }

    public String getRussian() {
        return translation;
    }
}