package com.users;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigInteger;
import java.util.Locale;

@JsonAutoDetect
public abstract class User {

    String name;
    String surname;
    String lastname;

    String numberOfCard;
    BigInteger numberOfAccount;
    double availableAmountOfMoney;

    public User(){}

    @JsonProperty("ФИО")
    public String getFullName() {
        return surname + " "+ name + " " + lastname;
    }

    @JsonIgnore
    public void setName(String name) {
        this.name = name;
    }

    @JsonIgnore
    public void setSurname(String surname) {
        this.surname = surname;
    }

    @JsonIgnore
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getNumberOfCard() {
        return numberOfCard;
    }

    @JsonIgnore
    public void setNumberOfCard(String numberOfCard) {
        this.numberOfCard = numberOfCard;
    }

    @JsonIgnore
    public double getMoney() {
        return availableAmountOfMoney;
    }

    @JsonIgnore
    public void setMoney(double availableAmountOfMoney) {
        this.availableAmountOfMoney = availableAmountOfMoney;
    }

    public BigInteger getNumberOfAccount() {
        return numberOfAccount;
    }

    @JsonIgnore
    public void setNumberOfAccount(BigInteger numberOfAccount) {
        this.numberOfAccount = numberOfAccount;
    }

    @JsonIgnore
    public String getFullNameInLowerCase() {
        return getFullName().toLowerCase(Locale.ROOT);
    }

    @JsonIgnore
    public String getFullNameInUpperCase() {
        return getFullName().toUpperCase(Locale.ROOT);
    }
}
