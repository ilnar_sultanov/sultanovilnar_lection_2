package com.users;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigInteger;

@JsonAutoDetect
public class Sender extends User {

    public Sender(String surname, String name, String lastname, String numberOfCard, double availableAmountOfMoney, BigInteger numberOfAccount) {
        this.surname = surname;
        this.name = name;
        this.lastname = lastname;
        this.numberOfCard = numberOfCard;
        this.availableAmountOfMoney = availableAmountOfMoney;
        this.numberOfAccount = numberOfAccount;
    }

    @JsonIgnore
    public boolean isMoneyMoreThan(double moneyAmount) {
        return availableAmountOfMoney >= moneyAmount;
    }

    @JsonProperty("Номер карты отправителя")
    public String getNumberOfCard() {
        return numberOfCard;
    }

    @JsonProperty("Номер счета отправителя")
    public BigInteger getNumberOfAccount() {
        return numberOfAccount;
    }

}
