package com.users;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigInteger;

@JsonAutoDetect
public class Reciever extends User{

    public Reciever(String surname, String name, String lastname, String numberOfCard, double availableAmountOfMoney, BigInteger numberOfAccount) {
        this.surname = surname;
        this.name = name;
        this.lastname = lastname;
        this.numberOfCard = numberOfCard;
        this.availableAmountOfMoney = availableAmountOfMoney;
        this.numberOfAccount = numberOfAccount;
    }

    @JsonProperty("Номер карты получателя")
    public String getNumberOfCard() {
        return numberOfCard;
    }

    @JsonProperty("Номер счета получателя")
    public BigInteger getNumberOfAccount() {
        return numberOfAccount;
    }
}
