package com;

import com.transfers.Currency;
import com.transfers.Transfer;
import com.transfers.TransferFromAccountToAccount;
import com.transfers.TransferFromCardToCard;
import com.users.Reciever;
import com.users.Sender;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Main {
    public static void main(String[] args) {

        Sender petrov = new Sender("Петров", "Петр", "Петрович",
                "444B44",
                4000,
                new BigInteger("5788655")
        );
        Sender ismail = new Sender("Исмаилов", "Исмаил", "Исмаилович",
                "777A77",
                99000,
                new BigInteger("6787227")
        );


        Reciever ivanov = new Reciever("Иванов", "Иван", "Иванович",
                "555F55",
                3000,
                new BigInteger("3688665")
        );
        Reciever nikolaev = new Reciever("Николаев", "Николай", "Николаевич",
                "888K88",
                2000,
                new BigInteger("7965634")
        );


        Transfer firstTransfer = new TransferFromCardToCard(
                650,
                Currency.DOLLAR,
                new Date(),
                ismail,
                ivanov
        );
        firstTransfer.doTransfer();
        System.out.println(firstTransfer.getJsonString() + "\n");


        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) { e.printStackTrace();}


        Transfer secondTransfer = new TransferFromAccountToAccount(
                1000,
                Currency.RUBLE,
                new Date(),
                petrov,
                nikolaev
        );
        secondTransfer.doTransfer();
        System.out.println(secondTransfer.getJsonString());

        System.out.println("\nПолучение ФИО отправителя и получателя в верхнем и нижнем регистрах:");
        System.out.println(ismail.getFullNameInUpperCase());
        System.out.println(nikolaev.getFullNameInLowerCase());

    }
}
